# ChatGPT CLI

[chatgpt-cli](https://github.com/marcolardera/chatgpt-cli)

<!-- metadata -->

* **Category**: Utilities
* **Status**: 0, work-in-progress
* **Image**: , 4, local
* **Healthcheck**: No
* **Backups**: N/A
* **Email**: N/A
* **Tests**: No
* **SSO**: N/A

<!-- endmetadata -->

# Deployment

Copy the Dockerfile to the server and build it:
```
docker build -t chatgpt .
```
