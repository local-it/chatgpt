export CONFIG_VERSION=v1

message() {
    python3 -c """
import json
import os
lastfile = sorted(os.listdir('/root/.config/chatgpt-cli/session-history/'))[-1]
message = json.load(open(f'/root/.config/chatgpt-cli/session-history/{lastfile}'))
print(message['messages'][-1]['content'])
    """

}
