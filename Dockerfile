FROM python:3.12-alpine

RUN apk add --upgrade --no-cache git vim bash

RUN pip install --upgrade pip

RUN pip install --no-build-isolation git+https://github.com/marcolardera/chatgpt-cli

ENTRYPOINT [ "tail", "-f", "/dev/null" ]
